//Abbiamo creato una classe `Ristorante` per gestire il menu, gli ordini dei clienti e le prenotazioni.
import java.util.ArrayList;
import java.util.List;

class Ristorante {
    private List<Piatto> menu;
    private List<Ordine> ordini;
    private List<Cliente> clienti;

    public Ristorante() {
        menu = new ArrayList<>();
        ordini = new ArrayList<>();
        clienti = new ArrayList<>();
    }

    // Aggiungere un piatto al menu
    public void aggiungiPiatto(Piatto piatto) {
        menu.add(piatto);
    }

    // Registra un ordine
    public void registraOrdine(Ordine ordine) {
        ordini.add(ordine);
    }

    // Registra un cliente
    public void registraCliente(Cliente cliente) {
        clienti.add(cliente);
    }

    // Getter per il menu
    public List<Piatto> getMenu() {
        return menu;
    }

    // Getter per gli ordini
    public List<Ordine> getOrdini() {
        return ordini;
    }

    // Getter per i clienti
    public List<Cliente> getClienti() {
        return clienti;
    }

    // Setter per il menu
    public void setMenu(List<Piatto> menu) {
        this.menu = menu;
    }

    // Setter per gli ordini
    public void setOrdini(List<Ordine> ordini) {
        this.ordini = ordini;
    }

    // Setter per i clienti
    public void setClienti(List<Cliente> clienti) {
        this.clienti = clienti;
    }

    @Override
    public String toString() {
        return "Menu: " + menu + "\nOrdini: " + ordini + "\nClienti: " + clienti;
    }
}
