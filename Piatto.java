/* Abbiamo creato un Codice JAVA IDEA :(ricorda di fare anche il make file)
Gestione di un Ristorante:
   - Abbiamo Creato una classe `Piatto` per rappresentare i piatti disponibili nel menu con attributi come nome, 
   prezzo, ingredienti, ecc.
   - Abbiamo Creato una classe `Ristorante` per gestire il menu, gli ordini dei clienti e le prenotazioni.
   - Abbiamo Creato una classe `Cliente` per rappresentare i clienti del ristorante con informazioni personali
   (nome,cognome ecc..).
   - Abbiamo Creatouna classe `Ordine` per gestire gli ordini dei clienti e i piatti richiesti.
   - Abbiamo Creato una classe `Main` che simula l'esperienza di un cliente nel ristorante
   -infine deve mandare tutto in output */
   
/*Abbiamo Creato una classe `Piatto` per rappresentare i piatti disponibili nel menu con attributi come nome, 
prezzo, ingredienti, ecc.*/
import java.util.List;

class Piatto {
    private String nome;
    private double prezzo;
    private List<String> ingredienti;
    private String descrizione;

    public Piatto(String nome, double prezzo, List<String> ingredienti, String descrizione) {
        this.nome = nome;
        this.prezzo = prezzo;
        this.ingredienti = ingredienti;
        this.descrizione = descrizione;
    }

    // Getter per il nome
    public String getNome() {
        return nome;
    }

    // Getter per il prezzo
    public double getPrezzo() {
        return prezzo;
    }

    // Getter per gli ingredienti
    public List<String> getIngredienti() {
        return ingredienti;
    }

    // Getter per la descrizione
    public String getDescrizione() {
        return descrizione;
    }

    // Setter per il nome
    public void setNome(String nome) {
        this.nome = nome;
    }

    // Setter per il prezzo
    public void setPrezzo(double prezzo) {
        this.prezzo = prezzo;
    }

    // Setter per gli ingredienti
    public void setIngredienti(List<String> ingredienti) {
        this.ingredienti = ingredienti;
    }

    // Setter per la descrizione
    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }
    // Metodo di copia per la classe Piatto
    public Piatto copiaPiatto() {
        List<String> copiaIngredienti = new ArrayList<>(this.ingredienti);
        return new Piatto(this.nome, this.prezzo, copiaIngredienti, this.descrizione);
    }
    @Override
    public String toString() {
        return "Nome: " + nome + "\nPrezzo: " + prezzo + "\nIngredienti: " + ingredienti + "\nDescrizione: " + descrizione;
    }
}
