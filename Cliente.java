//Abbiamo creato una classe `Cliente` per rappresentare i clienti del ristorante con informazioni personali.
class Cliente {
    private String nome;
    private String cognome;
    private String numeroTelefono;

    public Cliente(String nome, String cognome, String numeroTelefono) {
        this.nome = nome;
        this.cognome = cognome;
        this.numeroTelefono = numeroTelefono;
    }

    // Getter nome cliente
    public String getNome() {
        return nome;
    }

    // Getter cognome cliente
    public String getCognome() {
        return cognome;
    }

    // Getter NumeroTelefono del cliente
    public String getNumeroTelefono() {
        return numeroTelefono;
    }

    // Setter nome del cliente
    public void setNome(String nome) {
        this.nome = nome;
    }

    // Setter Cognome del cliente
    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    // Setter numero telefono del cliente
    public void setNumeroTelefono(String numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }
}

