//Abbiamo creato una classe `Ordine` per gestire gli ordini dei clienti e i piatti richiesti.
import java.util.List;
import java.util.ArrayList;

class Ordine {
    private Cliente cliente;
    private List<Piatto> piattiOrdinati;

    public Ordine(Cliente cliente) {
        this.cliente = cliente;
        this.piattiOrdinati = new ArrayList<>();
    }

    // Getter per il cliente
    public Cliente getCliente() {
        return cliente;
    }

    // Getter per i piatti ordinati
    public List<Piatto> getPiattiOrdinati() {
        return piattiOrdinati;
    }

    // Aggiungi un piatto all'ordine
    public void aggiungiPiatto(Piatto piatto) {
        piattiOrdinati.add(piatto);
    }

    // Rimuovi un piatto dall'ordine
    public void rimuoviPiatto(Piatto piatto) {
        piattiOrdinati.remove(piatto);
    }

    // Calcola il totale dell'ordine
    public double calcolaTotale() {
        double totale = 0.0;
        for (Piatto piatto : piattiOrdinati) {
            totale += piatto.getPrezzo();
        }
        return totale;
    }
}

