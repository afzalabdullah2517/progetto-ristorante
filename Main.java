//Abbiamo creato una classe `Main` che simula l'esperienza di un cliente nel ristorante
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Ristorante ristorante = new Ristorante();

        //Patti del ristorante
        Piatto p1 = new Piatto("Parfait", 3.99, List.of("Mandorle", "Latte", "Zucchero", "Cioccolato fondente fuso"), "Dessert tradizionale siciliano");
        Piatto p2 = new Piatto("Pizza Margherita", 7, List.of("Pomodoro", "Salsa", "Mozzarella"), "Pizza classica con pomodoro e mozzarella,piatto tradizionale Italian");
        Piatto p3 = new Piatto("Biryani", 5.50, List.of("Riso", "Pollo", "Spezie piccanti"), "Piatto tradizionale del Pakistan");
        
        ristorante.aggiungiPiatto(p1);
        ristorante.aggiungiPiatto(p2);
        ristorante.aggiungiPiatto(p3);

        //nomi dei clienti e numeri di telefono 
        Cliente cliente1 = new Cliente("Alessia", "Lombardo", "000-000-0000");
        Cliente cliente2 = new Cliente("Hania", "Butt", "123-456-7890");
        Cliente cliente3 = new Cliente("Abdullah", "Afzal", "987-654-3210");

        ristorante.registraCliente(cliente1);
        ristorante.registraCliente(cliente2);
        ristorante.registraCliente(cliente3);

        List<Piatto> ordineCliente1 = List.of(p1, p2, p3);
        Ordine ordine1 = new Ordine(cliente1, ordineCliente1);
        ristorante.registraOrdine(ordine1);

        List<Piatto> ordineCliente2 = List.of(p2);
        Ordine ordine2 = new Ordine(cliente2, ordineCliente2);
        ristorante.registraOrdine(ordine2);

        List<Piatto> ordineCliente3 = List.of(p3);
        Ordine ordine3 = new Ordine(cliente3, ordineCliente3);
        ristorante.registraOrdine(ordine3);

        // Menu del ristorante
        System.out.println("Menu del Ristorante:");
        for (Piatto piatto : ristorante.getMenu()) {
            System.out.println(piatto.getNome() + " - Prezzo: $" + piatto.getPrezzo());
        }

        // Ordini
        System.out.println("\nOrdini registrati:");
        for (Ordine ordine : ristorante.getOrdini()) {
            System.out.println("Cliente: " + ordine.getCliente().getNome());
            System.out.println("Piatti ordinati:");
            for (Piatto piatto : ordine.getPiattiOrdinati()) {
                System.out.println(piatto.getNome() + " - Prezzo: $" + piatto.getPrezzo());
            }
            System.out.println();
        }
    }
}
