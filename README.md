# Ristorante - Progetto JAVA
## Componenti Gruppo: Alessia Lombardo, Hania Butt e Abdullah Afzal*/

* ha 5 classi → Piatto, Ristorante, Cliente, Ordine e Main

* Interfaccia e terminale → il nostro codice ha una parte di interfaccia a terminale che mostra il menu del ristorante e gli ordini registrati.

* Descrizione codice Piatto 
            - è composta da : 
            - Nome → Una stringa che rappresenta il nome del piatto
        
            - Prezzo → Ha un valore double che rappresenta il prezzo del piatto
        
            - Ingredienti → Una lista di stringe che rappresenta gli ingredienti usati per la preparazione del piatto 
        
            - Descrizione → Una stringa che da una descrizione del piatto

            -  La classe Piatto ha i metodi…
            - Costruttore: Il costruttore ha come argomenti il nome, il prezzo, la lista di ingredienti e la descrizione del piatto e inizializza le proprietà dell'oggetto.
                  
            - Getter: restituisce il valore del oggetto (nome, prezzo, ingredienti, descrizione).
                  
            - Setter: consente di impostare il valore, esempio setNome(Stirng nome)  consente di impostare il valore di “nome” (nome, prezzo, ingredienti, descrizione).

            - il metodo copiaPiatto -> crea una nuova istanza di Piatto con gli stessi valori dei campi della classe. Abbiamo creato una nuova lista ArrayList per gli ingredienti invece di condividere la stessa lista con l'oggetto originale,
            in modo da evitare modifiche indesiderate 
                  
            - toString():  restituisce una rappresentazione testuale dell'oggetto, cioè il nome, il prezzo, gli ingredienti e la descrizione del piatto  e @Override indica che il metodo toString() sovrascrive un metodo predefinito della superclasse, personalizzando la rappresentazione testuale dell'oggetto.(ti permette di personalizzare il comportamento di un metodo e ottenere una rappresentazione testuale che desideri)
    
                  
            - Ad esempio, se crei un oggetto Piatto chiamato "Pizza Margherità" con un prezzo di 7 euro, una lista di ingredienti che include "salsa di pomodoro" e "Mozarella" e una descrizione come "Pizza classica", puoi ottenere le informazioni del piatto usando i metodi getter e toString()

* Descrizione codice Ristornate
            - è composto  da….
            - menu → Una lista di oggetti Piatto che rappresenta il menu del ristorante
        
            - Ordini →  Una lista di oggetti Ordine che rappresenta gli ordini effettuati al ristorante.
        
            - Clienti → Ha una lista di oggetti Cliente che rappresenta i clienti registrati (presso il ristorante)
              
            - La classe Ristornate ha i metodi... 
            - La classe ha un costruttore, metodi per aggiungere un piatto al menu e per registrare un ordine e registrare un cliente
        
            - ha i metodi getter e setter per menu,ordini e clienti e c'è un metodo toString() che restituirà una rappresentazione testuale delle informazioni riferite al ristorante, compreso il menu, gli ordini e i clienti.
        
* Descrizione codice Cliente
            - è composto da…
            - nome →  Una stringa che rappresenta il nome del cliente.
        
            - Cognome →  Una stringa che rappresenta il cognome del cliente.
        
            - NumeroTelefono →  Una stringa che rappresenta il numero di telefono del cliente.
              
            - La classe Cliente ha i metodi...
            - Costruttore: Il costruttore ha come argomenti il nome, il cognome e il numero di telefono del cliente e le inizializza

            - Getter: Ci sono tre getter(per nome,cognome e numero di telefono del cliente) 
                  
            - Setter: Ci sono tre setter(per nome,cognome e numero di telefono del cliente) 
          
            - La classe Cliente è progettata per rappresentare le informazioni dei clienti,  con il nome, il cognome e il numero di telefono del cliente. 
        
*  Descrizione codice Ordine
            - è composta da...
            - Cliente →  Un oggetto di tipo Cliente che rappresenta il cliente che ha effettuato l'ordine.
        
            -  PiattiOrdinati →  Una lista di oggetti Piatto che rappresenta i piatti inclusi nell'ordine
              
            - La classe Ordine ha i metodi...
            - Costruttore: Il costruttore ha un oggetto Cliente come argomento e inizializza l'ordine associandolo al cliente specifico. 
              
            - Getter per il cliente: Il metodo getCliente() restituisce l'oggetto Cliente associato a quell'ordine.
              
            - Getter per i piatti ordinati: Il metodo getPiattiOrdinati() restituisce la lista dei piatti che fanno parte dell'ordine.
              
            - Metodo aggiungiPiatto(Piatto piatto): 
                Questo metodo consente di aggiungere un piatto all'ordine. Il piatto specificato viene aggiunto alla lista dei piatti ordinati.
              
            - Metodo rimuoviPiatto(Piatto piatto): 
                Questo metodo consente di rimuovere un piatto dall'ordine. Il piatto specificato viene rimosso dalla lista dei piatti ordinati.  

            - Metodo calcolaTotale(): 
                Questo metodo calcola il totale dell'ordine sommando i prezzi di tutti i piatti inclusi nell'ordine quindi restituisce il totale come un numero decimale (double).
              
            - La classe Ordine l’abbiamo progettata per gestire le informazioni dei ordini richiesti dai clienti, compresi i piatti inclusi nell'ordine e il totale dell'ordine.  
      
*  Descrizione codice Main
            - è composto da…
            - Abbiamo creato un oggetto Ristorante chiamato ristorante.
              
            - Creazione dei piatti del ristorante :
                Abbiamo creato tre oggetti Piatto (p1, p2, p3) rappresentanti i piatti disponibili nel menu del ristorante. ogni piatto ha un nome, un prezzo, una lista di ingredienti e una descrizione.
              
            - Aggiunta dei piatti al menu:
                I piatti che abbiamo creato vengono aggiunti al menu del ristorante utilizzando il metodo aggiungiPiatto(Piatto piatto).
              
            - Creazione dei clienti:
                Vengono creati tre oggetti Cliente (cliente1, cliente2, cliente3) rappresentanti dai clienti del ristorante. Ogni cliente ha un nome, un cognome e un numero di telefono.
              
            - Registrazione dei clienti:
                I clienti creati vengono registrati dal il ristorante usando il metodo registraCliente(Cliente cliente).
              
            - Creazione e registrazione degli ordini:
                Vengono creati tre ordini (ordine1, ordine2, ordine3) per i clienti registrati, oguno contenente una lista di piatti che il cliente ha ordinato. Gli ordini vengono registrati utilizzando il metodo registraOrdine(Ordine ordine).
        
            - Visualizzazione del menu del ristorante:
                Viene stampato il menu del ristorante, mostrando i nomi e i prezzi di ciascun piatto.
              
            - Visualizzazione degli ordini registrati:
                Vengono stampati gli ordini registrati, inclusi i nomi dei clienti e i piatti che hanno ordinato, insieme ai prezzi.

* Istanze :
    - Per la classe Piatto abbiamo creato tre istanza: p1,p2 e p3

    - Per la classe Cliente abbiamo creato tre istante: Cliente 1, Cliente 2, Cliente 3

    - Per la classe Ordine abbiamo creato tre istante: abbiamo creato un'istanza di una lista di piatti, piattiOrdinati, inizializzata come un oggetto ArrayList, un'istanza di un cliente, inizializzata nel costruttore dell'ordine e un'istanza di una variabile double, totale, usata per calcolare il totale dell'ordine all'interno del metodo calcolaTotale.


    - In molti dei codici abbiamo usato il metodo toString() che restituisce una stringa che può essere considerata come la "rappresentazione testuale" dell'oggetto su cui è invocato (da usare ad esempio nella stampa). 


    - Le classi “Piatto”, “Ristorante”, “Cliente”, “Ordine” e “Main”  li abbiamo realizzati tutti insieme (Lombardo,Butt e Afzal) aiutandoci a vicenda in caso di dubbi 

    
